package com.franciscocalaca.backendcrud.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.franciscocalaca.backendcrud.entity.Produto;

@Repository
public interface ProdutoDao extends JpaRepository<Produto, Integer>{

	List<Produto> findByNome(String nome);
	
}
