package com.franciscocalaca.backendcrud.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.Type;

@Entity
public class Aluno extends EntityBase{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String nome;
	
	@Type(type = "jsonb")
	@Column(columnDefinition = "jsonb")
	private Map<String, Object> documentos = new HashMap<String, Object>();

	
	@ManyToMany
	private List<Materia> materias = new ArrayList<Materia>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Materia> getMaterias() {
		return materias;
	}

	public void setMaterias(List<Materia> materias) {
		this.materias = materias;
	}

	public Map<String, Object> getDocumentos() {
		return documentos;
	}

	public void setDocumentos(Map<String, Object> documentos) {
		this.documentos = documentos;
	}

	
}
