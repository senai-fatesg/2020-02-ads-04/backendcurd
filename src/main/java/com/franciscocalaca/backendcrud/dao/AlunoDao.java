package com.franciscocalaca.backendcrud.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.franciscocalaca.backendcrud.entity.Aluno;

@Repository
public interface AlunoDao extends JpaRepository<Aluno, Long>{

}
